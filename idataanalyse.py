#!/usr/bin/python3

#This is the cli version of the iDataAnalyse app

import json
from PIL import Image
from pprint import pprint
import argparse

def calculate_values_required():
    pass

def analyse(args):
    args = (args.analyse).split(",")
    fetch_database = pull_database()
    data = fetch_database["raw_data"][args[1]]
    icr = data["vertical_pixel_increment"]
    if args[0] == "FRB":
        l = []
        avg = list()
        #print(data.values())
        ctr = 0
        for x in data.values():
            ctr=ctr+1
            try:
                p = x[0]
            except:
                continue
            try:
                l.append(x[0])
            except:
                break
        s = l[99:]
        avg = sum(s) / len(s)
        v = 0
        for a in s:
            r = avg-a
            if avg < a:
                r = a-avg
            v = v + r
        v = v / icr
        indices = []
        for i in range(0, len(l)-len(s)+1):
            temp_array = l[i:i+len(s)]
            if temp_array == s:
                indices.append(i)
        if len(indices) > 1 and v > 50:
            return "Pulsating variable"
        else:
            return "Not a pulsating variable"
    elif args[0] == "Planethunt":
        l = []
        #avg = list()
        for x in data.values():
            try:
                s = x[0]
            except:
                break
            #avg = avg.append(x[0])
            l.append(x[0])
        s = l[:49]
        avg = sum(s) / len(s)
        v = 0
        for a in s:
            r = avg-a
            if avg < a:
                r = a-avg
            v = v + r
        v = v / icr
        indices = []
        for i in range(0, len(l)-len(s)+1):
            temp_array = l[i:i+len(s)]
            if temp_array == s:
                indices.append(i)
        if len(indices) > 1 and v > 50:
            return "Pulsating variable"
        else:
            return "Not a pulsating variable"
def img_to_raw(args):
    args = args.makeraw
    print(args)
    args = args.split(",")
    img = args[0]
    name = args[1]
    vertical_low = args[2]
    vertical_high = args[3]
    horizontal_low = args[4]
    horizontal_high = args[5]
    cperrow = int(args[6]) + 1
    im = Image.open(img)
    px = im.load()
    dct = dict()
    vertical_low = int()
    vertical_high = 450
    horizontal_low = 0
    horizontal_high = 900
    width, height = im.size
    vertical_pixel_increment = (vertical_high - vertical_low) / height
    print(vertical_pixel_increment)
    horizontal_pixel_increment = (horizontal_high - horizontal_low) / width
    base_h = horizontal_low
    for a in range(1, width):
        print(a)
        found = list()
        base = vertical_low
        lst = list()
        for x in range(int(), cperrow):
            for b in range(1, height):
                print(px[a,b])
                if px[a, b] not in found and px[a, b] != (int(),int(),int()):
                    found.append(px[a,b])
                    lst.append(base)
                    break
                base = base + vertical_pixel_increment
        lst.pop(0)
        dct[str(base_h)] = lst
        base_h = base_h + horizontal_pixel_increment
    ct = sorted(dct, key=float)
    dc = dct.values()
    dct = dict()
    for x,y in zip(ct, dc):
        x = f"{x}"
        dct[x] = y
    dct["horizontal_pixel_increment"]=horizontal_pixel_increment
    dct["vertical_pixel_increment"]=vertical_pixel_increment
    x = dict()
    with open ("./config.json", "r") as f:
        x = json.load(f)
        f.close()
    x["raw_data"][f"{name}"] = dct
    with open ("./config.json", "w") as f:
        json.dump(x, f)
        f.close()
    dct["name"]=name
    return dct

def pull_database():
    with open("./config.json", "r") as f:
        return json.load(f)
        
def push_to_database():
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--analyse",
        "--analyse",
        type=str,
        default=str(),
        help="Analyse data: Usage: ",
    )
    parser.add_argument(
        "--makeraw",
        "--makeraw",
        type=str,
        default=str(),
        help="Usage: python3.8 idataanalyse.py --makeraw ~/Projects/iDataAnalyse/img.png img 0 900 0 360",
    )
    args = parser.parse_args()
    if args.makeraw:
        img_to_raw(args)
    if args.analyse:
        print(analyse(args))

"""
Database structure:

{
    "raw_data":{<raw_data_name>:<raw_data>}
    ,
    "analysis_criteria_template":{<analysis_criteria_name>:<analysis_criteria>}
    ,
    "result":{<result_name>:<result>}

}
"""