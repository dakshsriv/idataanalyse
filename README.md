# iDataAnalyse

This is a scientific analysis software that anyone can very easily contribute to.
You can just make a pull request with a description to write the algorithm for analysis. This version 1.0.0
has 2 in-built analysis algorithms, for analysing FRB's and searching for planets provided
with the correct data. You can also make a pull request with a description to update an algorithm. Your pull 
requests might be accepted and included in the next release!
